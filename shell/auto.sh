DISTRIBUTION='gentoo'
TERMINAL='alacritty'
BROWSER='librewolf'
WM='xmonad'
NOTIFICATIOND='dunst'
MUSICD='mpd'
COMPOSITOR='picom'
BAR='polybar'
PROMPT='powerline'
LAUNCHER='rofi'

if [ -d /bin/$TERMINAL ]
    then
        echo $TERMINAL 'is already installed.'
    elif [ -d /bin/emerge ]; then
        echo 'Yes' | emerge --ask x11-terms/alacritty 'Installing' $TERMINAL'.' >&-
    else
        echo 'An error has been encountered whilst trying to install' $TERMINAL'.'
fi;

if [ -d /bin/$BROWSER ]
    then
        echo $BROWSER 'is already installed.'
    elif [ -d /bin/eselect ]; then
        echo 'Yes' | eselect repository add librewolf git https://gitlab.com/librewolf-community/browser/gentoo.git && emaint -r librewolf sync 'Installing' $BROWSER'.' >&-
    else
        echo 'An error has been encountered whilst trying to install' $BROWSER'.'
fi;

if [ -d /bin/$WM ]
    then
        echo $WM 'is already installed.'
    elif [ -d /bin/emerge ]; then
        echo 'Yes' | emerge --ask x11-wm/xmonad 'Installing' $WM'.' >&-
    else
        echo 'An error has been encountered whilst trying to install' $WM'.'
fi;

if [ -d /bin/$NOTIFICATIOND ]
    then
        echo $NOTIFICATIOND 'is already installed.'
    elif [ -d /bin/emerge ]; then
	echo 'Yes' | emerge --ask --verbose x11-misc/dunst 'Installing' $NOTIFICATIOND'.' >&-
    else
	echo 'An error has been encountered whilst trying to install' $NOTIFICATIOND'.'
fi;

if [ -d /bin/$MUSICD ]
    then
	echo $MUSICD 'is already installed.'
    elif [ -d /bin/emerge ]; then
	echo 'Yes' | emerge --ask media-sound/mpd 'Installing' $MUSICD'.' >&-
    else
	echo 'An error has been encountered whilst trying to install' $MUSICD'.'
fi;

if [ -d /bin/$COMPOSITOR ]
    then
	echo $COMPOSITOR 'is already installed.'
    elif [ -d /bin/emerge ]; then
	echo 'Yes' | emerge --ask x11-misc/picom 'Installing' $COMPOSITOR'.' >&-
    else
	echo 'An error has been encountered whilst trying to install' $COMPOSITOR'.'
fi;

if [ -d /bin/$BAR ]
    then
	echo $BAR 'is already installed.'
    elif [ -d /bin/emerge ]; then
	echo 'Yes' | emerge --ask x11-misc/polybar 'Installing' $BAR'.' >&-
    else
	echo 'An error has been encountered whilst trying to install' $BAR'.' 
fi;

if [ -d /bin/$PROMPT ]
    then
	echo $PROMPT 'is already installed.'
    elif [ -d /bin/emerge ]; then
	echo 'Yes' | emerge --ask app-shells/powerline 'Installing' $PROMPT'.' >&-
    else
	echo 'An error has been encountered whilst trying to install' $PROMPT'.'
fi;

if [ -d /bin/$LAUNCHER ]
    then
	echo $LAUNCHER 'is already installed.'
    elif [ -d /bin/emerge ]; then
	echo 'Yes' | emerge --ask x11-misc/rofi 'Installing' $LAUNCHER'.' >&-
    else
	echo 'An error has been encountered whilst trying to install' $LAUNCHER'.'
fi;
